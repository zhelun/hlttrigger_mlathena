#include "NTupleAlg.h"
#include "StoreGate/ReadHandle.h"
#include "PathResolver/PathResolver.h"

// Externals
#include "lwtnn/LightweightGraph.hh"
#include "lwtnn/NanReplacer.hh"
#include "lwtnn/parse_json.hh"

#include <istream>

NTupleAlg::NTupleAlg(const std::string &name, ISvcLocator *pSvcLocator) : 
  AthAnalysisAlgorithm(name, pSvcLocator),
  m_graphNN(nullptr),
  m_graphNNsingle(nullptr)
{
  declareProperty("TrigDecisionTool", m_tdt);

  std::ifstream inputNN(PathResolverFindCalibFile("MasterPerf/NNred.json"));
  lwt::GraphConfig configNN = lwt::parse_json_graph(inputNN);
  m_graphNN.reset(new lwt::LightweightGraph(configNN));
  if (configNN.inputs.size() != 1) {
    throw std::logic_error("only one input node allowed");
  }

  std::ifstream inputNNsingle(PathResolverFindCalibFile("MasterPerf/NNsingleLayerRed.json"));
  lwt::GraphConfig configNNsingle = lwt::parse_json_graph(inputNNsingle);
  m_graphNNsingle.reset(new lwt::LightweightGraph(configNNsingle));
  if (configNNsingle.inputs.size() != 1) {
    throw std::logic_error("only one input node allowed");
  }
}

NTupleAlg::~NTupleAlg() {}

StatusCode NTupleAlg::initialize()
{
  ATH_CHECK(m_triggerMETs.initialize());
  ATH_CHECK(m_tdt.retrieve());
  if (m_isEB)
    ATH_CHECK(m_ebTool.retrieve());
  else
    ATH_CHECK(m_truthMET.initialize());
  ATH_CHECK(m_evtInfo.initialize());
  ATH_CHECK(m_l1MET.initialize());
  ATH_CHECK(m_recoMET.initialize());
  const char *treeName = m_treeName.value().c_str();
  m_tree = bookGetPointer(TTree(treeName, treeName));
  if (!m_tree)
    return StatusCode::FAILURE;
  m_trigMet.reserve(m_triggerMETs.size());
  m_trigPhi.reserve(m_triggerMETs.size());
  m_trigSumEt.reserve(m_triggerMETs.size());
  for (const SG::ReadHandleKey<xAOD::TrigMissingETContainer> &key : m_triggerMETs)
  {
    std::string name = key.key();
    if (name.substr(0, 8) == "HLT_MET_")
      name = name.substr(8);
    m_trigMet.push_back(0.);
    m_tree->Branch((name + ".met").c_str(), &m_trigMet.back());
    m_trigPhi.push_back(0.);
    m_tree->Branch((name + ".phi").c_str(), &m_trigPhi.back());
    m_trigSumEt.push_back(0.);
    m_tree->Branch((name + ".sumet").c_str(), &m_trigSumEt.back());
  }
  m_trigDecisions.reserve(m_triggerNames.size());
  m_trigPrescales.reserve(m_trigPrescales.size());
  for (const std::string &trigger : m_triggerNames)
  {
    m_trigDecisions.push_back(false);
    m_tree->Branch((trigger + ".isPassed").c_str(), &m_trigDecisions.back());
    m_trigPrescales.push_back(1.0);
    m_tree->Branch((trigger + ".prescale").c_str(), &m_trigPrescales.back());
  }
  if (m_isEB)
  {
    m_tree->Branch("EBWeight", &m_ebWeight);
    m_tree->Branch("EBLiveTime", &m_ebLiveTime);
    m_tree->Branch("LBLumi", &m_lbLumi);
    m_tree->Branch("IsGoodLB", &m_isGoodLB);
    m_tree->Branch("IsUnbiasedEvent", &m_isUnbiasedEvent);
  }
  else
  {
    m_tree->Branch("MCEventWeight", &m_mcEventWeight);
    for (const std::string &term : {"NonInt", "Int", "IntMuons", "NonIntPlusIntMuons", "NonIntMinusIntMuons"})
    {
      m_tree->Branch(("Truth." + term + ".mpx").c_str(), &m_truthMpx[term]);
      m_tree->Branch(("Truth." + term + ".mpy").c_str(), &m_truthMpy[term]);
      m_tree->Branch(("Truth." + term + ".met").c_str(), &m_truthMet[term]);
      m_tree->Branch(("Truth." + term + ".sumet").c_str(), &m_truthSumEt[term]);
    }
  }
  m_tree->Branch("AverageInteractionsPerCrossing", &m_averageInteractionsPerCrossing);
  m_tree->Branch("ActualInteractionsPerCrossing", &m_actualInteractionsPerCrossing);
  m_tree->Branch("L1.met", &m_l1Met);
  m_tree->Branch("L1.phi", &m_l1Phi);
  m_tree->Branch("L1.sumet", &m_l1SumEt);
  m_tree->Branch("Reco.met", &m_recoMet);
  m_tree->Branch("Reco.phi", &m_recoPhi);
  m_tree->Branch("Reco.sumet", &m_recoSumEt);
 
  m_tree->Branch("NN_MET", &m_NN_MET);
  m_tree->Branch("NNsingle_MET", &m_NNsingle_MET);
  return StatusCode::SUCCESS;
}

StatusCode NTupleAlg::execute()
{
  m_trigMet.clear();
  m_trigPhi.clear();
  m_trigSumEt.clear();
  m_trigDecisions.clear();
  m_trigPrescales.clear();

  //for lwtnn:
  std::map<std::string, std::map<std::string, double> > inputs;
  for (const SG::ReadHandleKey<xAOD::TrigMissingETContainer> &key : m_triggerMETs)
  {
    std::string name = key.key();
    SG::ReadHandle<xAOD::TrigMissingETContainer> met = SG::makeHandle(key);
    if (met->size() == 0)
    {
      m_trigMet.push_back(-1);
      m_trigPhi.push_back(-1);
      m_trigSumEt.push_back(-1);
      //adding map pair:
      inputs["node_0"].insert(std::pair<std::string, double>(name+".met",-1) );
      inputs["node_0"].insert(std::pair<std::string, double>(name+".sumet",-1) );
    }
    else
    {
      const xAOD::TrigMissingET &metObj = *(met->at(0));
      float mpx = metObj.ex();
      float mpy = metObj.ey();
      m_trigMet.push_back(sqrt(mpx * mpx + mpy * mpy));
      m_trigPhi.push_back(atan2(mpy, mpx));
      m_trigSumEt.push_back(metObj.sumEt());
      //adding map pair:
      inputs["node_0"].insert(std::pair<std::string, double>(name+".met",sqrt(mpx * mpx + mpy * mpy)/1000.0) );
      inputs["node_0"].insert(std::pair<std::string, double>(name+".sumet",metObj.sumEt()/1000.0) );
    }
  }

  for (const std::string &trig : m_triggerNames)
  {
    m_trigDecisions.push_back(m_tdt->isPassed(trig));
    m_trigPrescales.push_back(m_tdt->getChainGroup(trig)->getPrescale());
  }

  SG::ReadHandle<xAOD::EventInfo> evtInfo = SG::makeHandle(m_evtInfo);
  if (m_isEB)
  {
    m_ebWeight = m_ebTool->getEBWeight(evtInfo.ptr());
    m_ebLiveTime = m_ebTool->getEBLiveTime(evtInfo.ptr());
    m_lbLumi = m_ebTool->getLBLumi(evtInfo.ptr());
    m_isGoodLB = m_ebTool->isGoodLB(evtInfo.ptr());
    m_isUnbiasedEvent = m_ebTool->isUnbiasedEvent(evtInfo.ptr());
  }
  else
  {
    m_mcEventWeight = evtInfo->mcEventWeight(0);
    SG::ReadHandle<xAOD::MissingETContainer> met = SG::makeHandle(m_truthMET);
    const xAOD::MissingET &nonIntMET = *((*met)["NonInt"]);
    const xAOD::MissingET &muonsMET = *((*met)["IntMuons"]);
    const xAOD::MissingET &intMET = *((*met)["Int"]);
    fillMET("Int", intMET);
    fillMET("NonInt", nonIntMET);
    fillMET("IntMuons", muonsMET);
    fillMET("NonIntPlusIntMuons", nonIntMET + muonsMET);
    fillMET("NonIntMinusIntMuons", nonIntMET - muonsMET);
  }
  m_actualInteractionsPerCrossing = evtInfo->actualInteractionsPerCrossing();
  m_averageInteractionsPerCrossing = evtInfo->averageInteractionsPerCrossing();

  SG::ReadHandle<xAOD::MissingETContainer> offlineMET = SG::makeHandle(m_recoMET);
  const xAOD::MissingET &finalTrkMET = *((*offlineMET)["FinalTrk"]);
  const xAOD::MissingET &muonsMET = *((*offlineMET)["Muons"]);
  xAOD::MissingET finalTrkNoMuMET = finalTrkMET - muonsMET;
  m_recoMet = finalTrkNoMuMET.met();
  m_recoPhi = finalTrkNoMuMET.phi();
  m_recoSumEt = finalTrkNoMuMET.sumet();

  SG::ReadHandle<xAOD::EnergySumRoI> l1 = SG::makeHandle(m_l1MET);
  float mpx = l1->energyX();
  float mpy = l1->energyY();
  m_l1Met = sqrt(mpx * mpx + mpy * mpy);
  m_l1Phi = atan2(mpy, mpx);
  m_l1SumEt = l1->energyT();

  //lwtnn
  std::map<std::string, double> outputNN = m_graphNN->compute(inputs); //std::map<std::string, double>
  m_NN_MET=outputNN["out_0"];

  std::map<std::string, double> outputNNsingle = m_graphNNsingle->compute(inputs); //std::map<std::string, double>
  m_NNsingle_MET=outputNNsingle["out_0"];
  
  m_tree->Fill();
  return StatusCode::SUCCESS;
}

void NTupleAlg::fillMET(const std::string &term, const xAOD::MissingET &met)
{
  m_truthMpx[term] = met.mpx();
  m_truthMpy[term] = met.mpy();
  m_truthMet[term] = met.met();
  m_truthSumEt[term] = met.sumet();
}

DECLARE_COMPONENT(NTupleAlg)
