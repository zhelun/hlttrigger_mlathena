import AthenaPoolCnvSvc.ReadAthenaPool
looper = theApp.EventLoop
if not hasattr(svcMgr, looper):
  svcMgr += getattr(CfgMgr, looper)()
getattr(svcMgr, looper).EventPrintoutInterval = 1000

if not hasattr(svcMgr, "THistSvc"):
    svcMgr += CfgMgr.THistSvc()
svcMgr.THistSvc.Output += ["TREE DATAFILE='mc_tree.root' OPT='RECREATE'"]

ToolSvc += CfgMgr.Trig__TrigDecisionTool("TrigDecisionTool")

anaSeq = CfgMgr.AthSequencer("AnalysisSequence")
athAlgSeq += anaSeq

mets = [
    "HLT_MET_tcpufit",
    "HLT_MET_cell",
    "HLT_MET_trkmht",
    "HLT_MET_pfopufit",
    "HLT_MET_mhtpufit_pf_subjesgscIS",
    "HLT_MET_pfsum_vssk"
    ]

triggers = [
    "HLT_xe110_tcpufit_L1XE50",
    "HLT_xe100_trkmht_xe85_tcpufit_xe65_cell_L1XE50",
    "HLT_xe95_trkmht_xe90_tcpufit_xe75_cell_L1XE50",
    "HLT_xe100_pfopufit_L1XE50",
    "HLT_xe100_cvfpufit_L1XE50",
    "HLT_xe100_mhtpufit_em_subjesgscIS_L1XE50",
    "HLT_xe100_mhtpufit_pf_subjesgscIS_L1XE50",
    "L1_XE50",
    ]

anaSeq += CfgMgr.NTupleAlg(
  IsEB = False,
  TruthMET = "MET_Truth",
  TrigDecisionTool = ToolSvc.TrigDecisionTool,
  TriggerMETs=mets,
  Triggers=triggers,
  RootStreamName = "TREE"
)
