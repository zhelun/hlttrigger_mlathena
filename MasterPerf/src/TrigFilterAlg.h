#ifndef MasterPerf_TrigFilterAlg_H
#define MasterPerf_TrigFilterAlg_H

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "AsgTools/IAsgTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "Gaudi/Property.h"

class TrigFilterAlg : public AthAnalysisAlgorithm
{
  public:
    TrigFilterAlg(const std::string& name, ISvcLocator* pSvcLocator);
    ~TrigFilterAlg();

    virtual StatusCode initialize() override;
    virtual StatusCode execute() override;
  private:
    ToolHandle<Trig::TrigDecisionTool> m_tdt{"Trig::TrigDecisionTool/TrigDecisionTool"};
    Gaudi::Property<std::vector<std::string>> m_triggers{
      this, "Triggers", {}, "The triggers to require"
    };
}; //> end class TrigFilterAlg

#endif //> !MasterPerf_TrigFilterAlg_H